[Documentation](https://bitbucket.org/lupuionut/tablebooking/wiki/Home) | [Download](https://bitbucket.org/lupuionut/tablebooking/downloads/)

# Changelog #
### 5.9.2 (January 2023)
* Changed. In the restaurant settings, you cannot set the close time before the opening.
* Changed. More code support for a plugin. 

### 5.9.1 (December 2022)
* Changed. Add basic plugin support in the component's code.

### 5.9 (November 2022)
* New. Added a simple anti spam mechanism that allows you to block certain emails.

### 5.8.1 (January 2022)
* New. Added table name in the exported csv.

### 5.8 (October 2021)
* Fixed. During the booking process, two or more clients select the same table. The first reservation that is finished is being stored in the database, but the other users also receive a success message at the end of their reservation process. This fix removes the success message and properly display an error message.
* Changed. Display the error message in a modal way.

### 5.7.2 (September 2021)
* Changed. THe table selection interface, it will now display also the table name
* New. Added support to send table name in the notification email using the %table% tag

### 5.7.1 (August 2021)
* Changed. On settings page, the values for starting and ending time are now generated using a timeframe of 5 minutes, instead of the default timeframe.

### 5.7 (July 2021)
* Changed. You can use the same user as restaurant administrator for multiple restaurants.
* Fixed. There was a problem selecting a special day defined as working if the day of the week was marked as free day.

### 5.6 (May 2021)
* Fixed. Some minor issues when using php 8.
* Fixed. When editing bookings with past date, save will not work.
* New. Added placeholder for date,hour,places,name for cron notifications.
* New. Added a button to confirm multiple bookings directly on the bookings list page.
* New. Display phone number under the client name on the bookings list page.
* New. Added Italian language for the front-end.

### 5.5.5 (August 2020)
* Changed. Date display inside notifications or reservations list is based on selected front-end default language.

### 5.5.4 (July 2020)
* Fixed. Front-end warning related to woff font icons.
* New. Dutch front-end language file. 
* Changed. Added "d F Y" format to the list of available time formats.

### 5.5.3 (May 2020)
* Changed. Display table name in the booking list, in the management area.

### 5.5.2 (April 2020)
* Changed. New update url.

### 5.5.1 (July 2019)
* Changed. Starting this month, TableBooking is available free of charge. That includes also future updates. For this reason, I've removed the update token and added a support button on the about page, so that you can support me via PayPal to continue working on this extension.

### 5.5 (January 2019)
* New. GDPR support. If enabled, a checkbox with a custom text will appear before submit button.
* Changed. Implementation of tables arrangement on the front-end management page was rebuilt. When updating to this version, your current tables arrangement will be deleted!
* Changed. Enable/disable display of phone and comments fields on the front-end search page. 

### 5.4.2 (September 2018)
* Add "successfully reserved" message to the language file
* Move the message over the reserve button

### 5.4.1 (August 2018)
* Fix. Version 5.4 contains a fix that was incomplete. This release fixes that.

### 5.4 (August 2018)
* Fixed. Error when selecting last hour of day. The start hour was set but the end hour was impossible to set.
* Changed. Autopopulate reservation form with name and email when user is logged in.
* Changed. Add table name in the svg that display table arangement in the management area.

### 5.3.7
* Changed. In the 5.3.6 the component release date was wrong. This version is a fix for that.

### 5.3.6
* Fixed. If the client email address had space(s) in front or after, the reservation could not be saved.

### 5.3.5
* New. implemented html format for notifications
* Changed. set time display until 24.00

### 5.3.4
* New. option to select first day of week. As default, until now, the calendar picker was set to display Sunday as first day of week. You can now choose between Sunday and Monday.
* Changed. replace ID with Places in the booking list. You can see directly in the list with bookings the number of places corresponding to each reservation.
* Changed. when using search calendar, you can now differentiate between free days and working days. Free days are using a different colour and cannot be selected.
* Fixed. In certain situations, when time was not selected, the reservation form was still appearing.

### 5.3.3
* New. You can define if you want to see the list of bookings in the management area from today onwards or from the oldest booking onwards (all bookings, default option).
* Fixed. Inside management area, in the booking list view, if no date was selected, when using any of the date navigation arrows, the date was not correctly displayed. 
* Fixed. Date was not properly changed in the main dashboard.
* Fixed. Issue when logout using a Joomla login module.

### 5.3.2
* Fixed. inside management area, during the special day definition. Whenever a new special date was created, the actual date was not correctly saved.
* Fixed. inside management area, during editing/adding a booking, the calendar was not properly displayed.

### 5.3.1
* Fixed. bug in the calendar selection

### 5.3
* New. "Save & New" for creation of table
* New. implemented the update system for the component. To enable the auto update you must add the "update token" in the "About" section from back-end. This token can be obtained from https://ionutlupu.me/your-desk/orders.html
* Changed. Modified the front-end search design. Added a new functionality. If all tables are reserved, the client will receive (if available) a list with few available hours. 
* Changed. modified visual elements in the front-end management

### 5.2
* New. support for notifications via cron jobs
* New. support for the reservation module. The new module can load the component into an iframe and provides you with more flexibility in the placement of component

### 5.1.1
* New. search filter for bookings in the front-end management area
* Fixed. selection date bug
* Fixed. add new booking bug in the front-end management area